package com.example.cryptlab3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Cryptlab3Application {

    public static void main(String[] args) {
        SpringApplication.run(Cryptlab3Application.class, args);
    }

}
