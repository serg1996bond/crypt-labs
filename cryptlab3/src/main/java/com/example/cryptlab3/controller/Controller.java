package com.example.cryptlab3.controller;

import com.example.cryptlab3.encoder.Decoder;
import com.example.cryptlab3.encoder.Encoder;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
@AllArgsConstructor
public class Controller {

    private final Encoder encoder;
    private final Decoder decoder;

    @GetMapping("encode")
    public String  encodeBlocks(@RequestParam short key, @RequestParam byte steps, @RequestParam byte[] blocks) {
        return Arrays.toString(encoder.encode(blocks, steps, key));
    }

    @GetMapping("decode")
    public String decodeBlocks(@RequestParam short key, @RequestParam byte steps, @RequestParam byte[] blocks) {
        return Arrays.toString(decoder.decode(blocks, steps, key));
    }

}
