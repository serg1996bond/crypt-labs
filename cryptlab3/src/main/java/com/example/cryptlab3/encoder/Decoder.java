package com.example.cryptlab3.encoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class Decoder {

    Logger logger = LoggerFactory.getLogger(getClass());

    public byte[] decode(byte[] blocks, byte steps, short key) {
        logger.info("decode start; blocks = {}, steps = {}, key = {}",
                Arrays.toString(blocks),
                steps,
                key);
        Util.checkBlockSize(blocks);
        for (byte i = (byte) (steps-1); i >= 0; i--) {
            blocks = decodeStep(blocks, (byte) (i + 1), key);
        }
        logger.info("decode end; result = {}", Arrays.toString(blocks));
        return blocks;
    }

    private byte[] decodeStep(byte[] blocks,byte step,short key) {
        logger.info("decodeStep start; blocks = {}, step = {}, key = {}",
                Arrays.toString(blocks),
                step,
                key);
        byte[] result = new byte[blocks.length];
        result[1] = (byte) (blocks[0] ^ Util.f(blocks[blocks.length-1], step, key));
        for (byte i = 1; i < blocks.length - 1; i++) {
            result[i+1] = blocks[i];
        }
        result[0] = blocks[blocks.length - 1];
        logger.info("decodeStep end; result = {}", Arrays.toString(result));
        return result;
    }
}
