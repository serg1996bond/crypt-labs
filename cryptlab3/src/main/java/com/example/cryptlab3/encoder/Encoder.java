package com.example.cryptlab3.encoder;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class Encoder {

    Logger logger = LoggerFactory.getLogger(getClass());

    public byte[] encode(byte[] blocks, byte steps, short key) {
        logger.info("encode start; blocks = {}, steps = {}, key = {}",
                Arrays.toString(blocks),
                steps,
                key);
        Util.checkBlockSize(blocks);
        for (byte i = 0; i < steps; i++) {
            blocks = encodeStep(blocks, (byte) (i + 1), key);
        }
        logger.info("encode end; result = {}", Arrays.toString(blocks));
        return blocks;
    }

    private byte[] encodeStep(byte[] blocks,byte step,short key) {
        logger.info("encodeStep start; blocks = {}, step = {}, key = {}",
                Arrays.toString(blocks),
                step,
                key);
        byte[] result = new byte[blocks.length];
        result[0] = (byte) (blocks[1] ^ Util.f(blocks[0], step, key));
        for (byte i = 1; i < blocks.length - 1; i++) {
            result[i] = blocks[i + 1];
        }
        result[blocks.length - 1] = blocks[0];
        logger.info("encodeStep end; result = {}", Arrays.toString(result));
        return result;
    }

}
