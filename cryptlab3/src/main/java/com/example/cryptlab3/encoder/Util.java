package com.example.cryptlab3.encoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Util {

    private static final Logger logger = LoggerFactory.getLogger("util");

    static byte ror(byte b, byte steps) {
        for (byte i = 0; i < steps; i++) {
            b = (byte) (((byte)(b >>> 1)&0x7F) | ((byte) ((b & 1)<<7)));
        }
        return b;
    }

    static byte rol(byte b, byte steps) {
        for (byte i = 0; i < steps; i++) {
            if ((b&0x80) == 0x80) {
                b = (byte) (b << 1 | 1);
            } else {
                b = (byte) (b << 1);
            }
        }
        return b;
    }

    static void checkBlockSize(byte[] blocks) {
        if (blocks.length < 2) {
            throw new RuntimeException("too little array");
        }
    }

    static byte f(byte x1im1, byte step,short key) {
        logger.info("f start; X1(i-1) = {}, step = {}; key = {}", x1im1,step,key&0xffff);
        byte keyL = (byte) (key >> 8);
        byte keyR = (byte) key;
        byte vi = (byte) (Util.ror(keyL, step) ^ Util.ror(keyR, step));
        byte result = rol((byte) (x1im1^vi),step);
        logger.info("f end; result = {}", result&0xFF);
        return result;
    }
}
