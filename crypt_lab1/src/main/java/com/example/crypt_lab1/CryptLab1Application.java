package com.example.crypt_lab1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CryptLab1Application {

    public static void main(String[] args) {
        SpringApplication.run(CryptLab1Application.class, args);
    }

}
