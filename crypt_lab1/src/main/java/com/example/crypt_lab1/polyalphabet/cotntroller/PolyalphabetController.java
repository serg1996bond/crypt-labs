package com.example.crypt_lab1.polyalphabet.cotntroller;

import com.example.crypt_lab1.polyalphabet.algorithm.PolyalhabethPhraseEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PolyalphabetController {

    private final String trueAbc =        "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЫЭЮЯ ";
    private final String firstEncodeABC = "БЮГЫЕЬЗШЙЦЛФНТПРСОУМХКЧИЩЖЪДЭВЯ АЁ";
    private final String secondEncodeABC ="СОУМКХЧИЩЖЪДЭВЯАБЮГ ЕЬЗШЙЦЁФНТПРЫЛ";
    private final String thirdEncodeABC = "МНОПРСТУФХЦЧШЩЪЫЬЭЮЯ АБВГДЕЁЖЗИЙКЛ";
    private final PolyalhabethPhraseEncoder encoder = new PolyalhabethPhraseEncoder(trueAbc,firstEncodeABC,secondEncodeABC,thirdEncodeABC);

    public PolyalphabetController() throws Exception {
    }

    @GetMapping("encode/{message}")
    public String encodeMessage(@PathVariable String message) {
        return encoder.encodeMessage(message);
    }

    @GetMapping("decode/{message}")
    public String decodeMessage(@PathVariable String message) {
        return encoder.decodeMessage(message);
    }

}
