package com.example.crypt_lab1.polyalphabet.algorithm;

public class PolyalhabethPhraseEncoder {

    private final String trueABC;
    private final String[] encodeABC;

    public PolyalhabethPhraseEncoder(String trueABC, String...encodeABC) throws Exception {
        for (String ABC : encodeABC) {
            if (ABC.length() != trueABC.length()) throw new Exception("wrong encode ABC length");
        }
        this.trueABC = trueABC;
        this.encodeABC = encodeABC;
    }

    public String encodeMessage(String message) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < message.length(); i++) {
            var encodeABCNum = i % encodeABC.length;
            var charForEncode = message.charAt(i);
            var encodedChar = encodeABC[encodeABCNum].charAt(trueABC.indexOf(charForEncode));
            sb.append(encodedChar);
        }
        return sb.toString();
    }

    public String decodeMessage(String message) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < message.length(); i++) {
            var decodeABCNum = i % encodeABC.length;
            var charForDecode = message.charAt(i);
            var decodedChar = trueABC.charAt(encodeABC[decodeABCNum].indexOf(charForDecode));
            sb.append(decodedChar);
        }
        return sb.toString();
    }

}
