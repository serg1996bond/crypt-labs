import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class ImageDecoder {

    public static void decode(String url) throws IOException {
        var imageBytes = Files.readAllBytes(Path.of(url));
        StringBuilder sb = new StringBuilder();
        for (int j = 1078 ; j < imageBytes.length; ) {
            int ch = 0;
            for (int i = 0; i < 16; i++) {
                var imageByte = imageBytes[j];
                byte bite = (byte) (imageByte & 1);
                ch <<= 1;
                ch |= bite;
                j++;
            }
            if ((char) ch == '$') break;
            sb.append((char) ch);
        }
        System.out.println(sb.toString());
    }

}
