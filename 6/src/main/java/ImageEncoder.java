import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class ImageEncoder {

    public static void encode(String url, String message) throws IOException {
        message += "$";
        var chars = message.toCharArray();
        var imageBytes = Files.readAllBytes(Path.of(url));
        int j = 1078;
        for (char ch : chars) {
            for (int i = 0; i < 16; i++) {
                char temp = ch;
                temp >>= 15 - i;
                temp &= 1;
                imageBytes[j] &= 0b11111110;
                imageBytes[j] |= temp;
                j++;
            }
        }
        var outStream = new BufferedOutputStream(new FileOutputStream(url));
        outStream.write(imageBytes);
        outStream.close();
    }

}
