import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String text = "съешь этих мягких французких булок, да выпей еще чаю!";
        String secret = "secret16bytekey!";
        var scanner = new Scanner(System.in);
        System.out.println("введите новый пароль");
        var passHash = Encoder.encrypt(scanner.nextLine(),secret);
        System.out.println("зашифрованный пароль: " + passHash);
        System.out.println("проверка пароля:");
        while (true) {
            if (passHash.equals(Encoder.encrypt(scanner.nextLine(), secret))) {
                System.out.println("пароль введен верно");
                break;
            }
            System.out.println("пароль введен неверно, попробуйте еще раз: ");
        }
        System.out.println("результат дешифровки пароля:");
        System.out.println(Encoder.decrypt(passHash, secret));
        System.out.println("при неверном ключе вылетит исключение:");
        System.out.println(Encoder.decrypt(passHash, "sekret16bytekey!"));
    }
}
