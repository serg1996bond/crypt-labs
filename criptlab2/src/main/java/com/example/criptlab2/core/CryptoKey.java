package com.example.criptlab2.core;

import lombok.Getter;

@Getter
public class CryptoKey {
    private final short a, c, t;

    public CryptoKey(short a, short c, short t) {
        this.a = a;
        this.c = c;
        this.t = t;
    }
}
