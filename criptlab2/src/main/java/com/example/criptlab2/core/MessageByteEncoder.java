package com.example.criptlab2.core;

import com.example.criptlab2.infrastructure.MaskArrayGenerator;

public class MessageByteEncoder {
    public static String endecode(String message, CryptoKey key) {
        var messageChars = message.toCharArray();
        MaskArrayGenerator mag = new MaskArrayGenerator(key);
        var byteMask = mag.getByteMask(message.length());
        var encodedBytesAsChar = new char[message.length()];
        for (int i = 0; i < message.length(); i++) {
            encodedBytesAsChar[i] = (char) ((messageChars[i] ^ byteMask[i]) & 0xFF);
        }
        return String.copyValueOf(encodedBytesAsChar);
    }
}
