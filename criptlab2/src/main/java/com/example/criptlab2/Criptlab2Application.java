package com.example.criptlab2;

import com.example.criptlab2.core.CryptoKey;
import com.example.criptlab2.core.MessageByteEncoder;

public class Criptlab2Application {

    public static void main(String[] args) {
        var key = new CryptoKey((short) 3,(short) 5,(short) 2);
        var encoded = MessageByteEncoder.endecode("AaZz190", key);
        System.out.println(encoded);
        var decoded = MessageByteEncoder.endecode(encoded, key);
        System.out.println(decoded);
    }

}
