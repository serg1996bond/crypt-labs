package com.example.criptlab2.infrastructure;

import com.example.criptlab2.core.CryptoKey;

public class MaskArrayGenerator {
    private short currentMask;
    private final CryptoKey key;

    public MaskArrayGenerator(CryptoKey key) {
        this.key = key;
        this.currentMask = key.getT();
    }

    private byte next(){
        currentMask = (short) ((key.getA()*currentMask + key.getC()) & 0xFF);
        return (byte) currentMask;
    }

    public byte[] getByteMask(int size) {
        var byteMask = new byte[size];
        for (int i = 0; i < size; i++) {
            byteMask[i] = this.next();
        }
        return byteMask;
    }
}
