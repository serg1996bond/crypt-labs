public abstract class TextHashGenerator {

    public static int generateHash(String text) {
        return text.chars().reduce((left, right) -> left ^ right).getAsInt();
    }

}
