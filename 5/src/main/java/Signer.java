import java.math.BigInteger;
import java.util.Random;

public class Signer {

    public static SignedText signText(Key key, String text) {
        System.out.println("Start sign");
        System.out.println(key);
        int textHash = TextHashGenerator.generateHash(text);
        if (BigInteger.valueOf(textHash).mod(key.getQ()).compareTo(BigInteger.ZERO) == 0) textHash = 1;
        System.out.println("hash = " + textHash);
        BigInteger k = null;
        BigInteger r1 = null;
        BigInteger s = null;
        boolean sCheck = false;
        while (!sCheck) {
            boolean r1Check = false;
            while (!r1Check) {
                k = bigIntegerRandom(BigInteger.ONE, key.getQ());
                BigInteger r = calculateR(key, k);
                r1 = r.mod(key.getQ());
                if (r1.compareTo(BigInteger.ZERO) != 0) r1Check = true;
            }
            s = calculateS(key, textHash, k, r1);
            if (s.compareTo(BigInteger.ZERO) != 0) sCheck = true;
        }
        System.out.println("k = " + k);
        System.out.println("r1 = " + r1);
        System.out.println("s = " + s);
        System.out.println("End Sign\n");
        return new SignedText(text, r1, s, key.getQ(), key.getP(), key.getY(), key.getA());
    }

    private static BigInteger calculateS(Key key, int textHash, BigInteger k, BigInteger r1) {
        return (key.getX().multiply(r1)).add(k.multiply(BigInteger.valueOf(textHash))).mod(key.getQ());
    }

    private static BigInteger calculateR(Key key, BigInteger k) {
        return key.getA().modPow(k, key.getP());
    }

    private static BigInteger bigIntegerRandom(BigInteger min, BigInteger max) {
        BigInteger bigInteger = max.subtract(min);
        Random random = new Random();
        int len = bigInteger.bitLength();
        BigInteger res = new BigInteger(len, random);
        if (res.compareTo(min) < 0)
            res = res.add(min);
        if (res.compareTo(bigInteger) >= 0)
            res = res.mod(bigInteger).add(min);
        return res;
    }

}
