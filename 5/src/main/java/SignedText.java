import java.math.BigInteger;

public class SignedText {

    private final String text;
    private final BigInteger r1;
    private final BigInteger s;
    private final BigInteger q;
    private final BigInteger p;
    private final BigInteger y;
    private final BigInteger a;

    public String getText() {
        return text;
    }

    public BigInteger getR1() {
        return r1;
    }

    public BigInteger getS() {
        return s;
    }

    public BigInteger getQ() {
        return q;
    }

    public BigInteger getP() {
        return p;
    }

    public BigInteger getY() {
        return y;
    }

    public BigInteger getA() {
        return a;
    }

    public SignedText(String text, BigInteger r1, BigInteger s, BigInteger q, BigInteger p, BigInteger y, BigInteger a) {
        this.text = text;
        this.r1 = r1;
        this.s = s;
        this.q = q;
        this.p = p;
        this.y = y;
        this.a = a;
    }

    @Override
    public String toString() {
        return "SignedText{" +
                "r1=" + r1 +
                ", s=" + s +
                ", q=" + q +
                ", p=" + p +
                ", y=" + y +
                ", a=" + a +
                '}';
    }
}
