import java.math.BigInteger;

public final class KeyGenerator {

    public static Key generate(BigInteger pStart, BigInteger qStart, BigInteger secret){
        BigInteger p = null;
        BigInteger q = null;
        BigInteger a = null;
        boolean aIsBad = true;
        while (aIsBad) {
            aIsBad = true;
            boolean qIsBad = true;
            while (qIsBad) {
                qIsBad = true;
                p = findFirstP(pStart);
                try {
                    q = findFirstQ(qStart, p);
                    qIsBad = false;
                } catch (Exception e) {
                    pStart = p.add(BigInteger.ONE);
                    qIsBad = true;
                }
            }
            try {
                a = findFirstA(p, q);
                aIsBad = false;
            } catch (Exception e) {
                aIsBad = true;
            }
        }
        BigInteger y = a.modPow(secret, p);
        return new Key(p,q,a,secret,y);
    }

    public static BigInteger findFirstP(BigInteger start){
        BigInteger temp = start;
        while (true) {
            if (primeCheck(temp)) break;
            temp = temp.add(BigInteger.ONE);
        }
        return temp;
    }

    public static BigInteger findFirstQ(BigInteger start, BigInteger p) throws Exception {
        BigInteger dividend = p.subtract(BigInteger.ONE);
        BigInteger temp = start;
        while (true) {
            if (isPrimeDivisor(temp, dividend)) break;
            temp = temp.add(BigInteger.ONE);
        }
        return temp;
    }

    public static BigInteger findFirstA(BigInteger p, BigInteger q) throws Exception {
        var a = BigInteger.TWO;
        while (true) {
            if (a.modPow(q, p).compareTo(BigInteger.ONE) == 0) break;
            a = a.add(BigInteger.ONE);
            if (a.compareTo(p.subtract(BigInteger.ONE))== 0) throw new Exception("overflow");
        }
        return a;
    }

    private static boolean primeCheck(BigInteger num) {
        BigInteger temp = new BigInteger("2");
        while (temp.compareTo(num) < 0) {
            if(num.mod(temp).compareTo(BigInteger.ZERO)==0)
                return false;
            temp = temp.add(BigInteger.ONE);
        }
        return true;
    }

    private static boolean isPrimeDivisor(BigInteger num, BigInteger dividend) throws Exception {
        if (num.compareTo(dividend.divide(BigInteger.TWO))>0) throw new Exception("out of possible values");
        if (dividend.mod(num).compareTo(BigInteger.ZERO)!=0) return false;
        return primeCheck(num);
    }

}
