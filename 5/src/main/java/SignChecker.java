import java.math.BigInteger;

public class SignChecker {

    public static void signCheck(SignedText st) {
        System.out.println("Check start");
        System.out.println(st.toString());
        if(st.getR1().compareTo(BigInteger.ZERO) <= 0 || st.getR1().compareTo(st.getQ()) >= 0)
            throw new RuntimeException("wrong r1");
        if(st.getS().compareTo(BigInteger.ZERO) <= 0 || st.getS().compareTo(st.getQ()) >= 0)
            throw new RuntimeException("wrong s");
        int textHash = TextHashGenerator.generateHash(st.getText());
        BigInteger textHashBI = BigInteger.valueOf(textHash);
        if (textHashBI.mod(st.getQ()).compareTo(BigInteger.ZERO) == 0) textHash = 1;
        System.out.println("textHash = " + textHash);
        BigInteger v = calculateV(st, textHashBI);
        System.out.println("v = " + v);
        BigInteger z1 = calculateZ1(st, v);
        System.out.println("z1 = " + z1);
        BigInteger z2 = calculateZ2(st, v);
        System.out.println("z2 = " + z2);
        //костыль так как нельзя возводить в степень BigInteger
        BigInteger u = calculateU(st, z1, z2);
        System.out.println("u = " + u);
        System.out.println("Check end");
    }

    private static BigInteger calculateU(SignedText st, BigInteger z1, BigInteger z2) {
        int z1Int = z1.intValue();
        return st.getA().pow(z1Int).multiply(st.getY().modPow(z2, st.getP())).mod(st.getQ());
    }

    private static BigInteger calculateV(SignedText st, BigInteger textHashBI) {
        return textHashBI.modPow(st.getQ().subtract(BigInteger.TWO), st.getQ());
    }

    private static BigInteger calculateZ1(SignedText st, BigInteger v) {
        return st.getS().multiply(v).mod(st.getQ());
    }

    private static BigInteger calculateZ2(SignedText st, BigInteger v) {
        return st.getQ().subtract(st.getR1()).multiply(v).mod(st.getQ());
    }

}
