import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Random;

public class Main {

    public static void main(String[] args) throws IOException {
        var key = KeyGenerator.generate(BigInteger.valueOf(2000), BigInteger.valueOf(20), BigInteger.valueOf(42));
        File file = new File("text.txt");
        if (!file.exists()) throw new RuntimeException("File not exists");
        String text = Files.readString(Path.of("text.txt"));
        var st = Signer.signText(key, text);
        SignChecker.signCheck(st);
    }




}
