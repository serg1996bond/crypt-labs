import java.math.BigInteger;

public class Key {

    private BigInteger p;
    private BigInteger q;
    private BigInteger a;
    private BigInteger x;
    private BigInteger y;

    public BigInteger getP() {
        return p;
    }

    public BigInteger getQ() {
        return q;
    }

    public BigInteger getA() {
        return a;
    }

    public BigInteger getX() {
        return x;
    }

    public BigInteger getY() {
        return y;
    }

    public Key(BigInteger p, BigInteger q, BigInteger a, BigInteger x, BigInteger y) {
        this.p = p;
        this.q = q;
        this.a = a;
        if (x.compareTo(BigInteger.ONE) <= 0 || x.compareTo(q) >= 0) throw new RuntimeException("wrong x");
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Key{" +
                "p=" + p +
                ", q=" + q +
                ", a=" + a +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
