package com.example.cryptlab4.encoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class KeyGenerator {

    Logger log = LoggerFactory.getLogger(getClass());

    public Map<String,Map<String,Integer>> generateKeys (int p, int q, int e, int d) {
        log.info("create encoder start");
        simpleCheck(p);
        log.info("p is OK");
        simpleCheck(q);
        log.info("q is OK");
        int n = p*q;
        int eilerFun = (p - 1) * (q - 1);
        simpleCheck(e);
        if (e > eilerFun) throw new RuntimeException("e will be less then eilerFun");
        mutuallySimpleCheck(e,eilerFun);
        log.info("e is OK");
        var dCheck = (d * e) % eilerFun;
        if (dCheck!=1) throw new RuntimeException("d * e % eilerFun = " + dCheck);
        log.info("d is OK");
        Map<String, Integer> secretKey = new HashMap<>(3);
        secretKey.put("d", d);
        secretKey.put("p", p);
        secretKey.put("q", q);
        Map<String, Integer> publicKey = new HashMap<>(2);
        publicKey.put("e", e);
        publicKey.put("n", n);
        Map<String, Map<String, Integer>> result = new HashMap<>(2);
        result.put("publicKey", publicKey);
        result.put("secretKey", secretKey);
        return result;
    }

    private void simpleCheck(int n) {
        for (int i = 2; i < n; i++) {
            if (n<0)
                throw new RuntimeException(String.format("%d is less then zero"));
            if (n%i == 0)
                throw new RuntimeException(String.format("%d is not simple, because it's can div to %d", n, i));
        }
    }

    private void mutuallySimpleCheck(int a, int b) {
        List<Integer> aDividers = new ArrayList<>();
        List<Integer> bDividers = new ArrayList<>();
        for (int i = 2; i <= a; i++) {
            if (a%i == 0) aDividers.add(i);
        }
        for (int i = 2; i <= b; i++) {
            if (b%i == 0) bDividers.add(i);
        }
        if(!Collections.disjoint(aDividers, bDividers))
            throw new RuntimeException(String.format("%d is not mutually simple to %d", a, b));
    }
}
