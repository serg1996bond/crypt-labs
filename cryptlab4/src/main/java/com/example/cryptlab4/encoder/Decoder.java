package com.example.cryptlab4.encoder;

import org.springframework.stereotype.Service;

import java.math.BigInteger;

@Service
public class Decoder {
    public String decodeString(String string, int d, int p, int q) {
        var chars = string.toCharArray();
        var decodedChars = new char[chars.length];
        for (int i = 0; i < chars.length; i++) {
            decodedChars[i] = decodeChar(chars[i],d,p,q);
        }
        return String.copyValueOf(decodedChars);
    }

    private char decodeChar(char ch,int d, int p, int q) {
        var bigIntCh = BigInteger.valueOf(ch);
        var bigChPowD = bigIntCh.pow(d);
        var bigP = BigInteger.valueOf(p);
        var bigQ = BigInteger.valueOf(q);
        var bigN = bigQ.multiply(bigP);
        var result = bigChPowD.mod(bigN);
        if (result.intValue()=='㽑'||result.intValue()=='㚋') System.out.println(result.toString());
        return (char) result.intValue();
    }
}
