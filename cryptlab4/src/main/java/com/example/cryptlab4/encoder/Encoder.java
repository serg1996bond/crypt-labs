package com.example.cryptlab4.encoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigInteger;

@Service
public class Encoder {

    private final Logger log = LoggerFactory.getLogger(getClass());

    public String encodeString(String string,int e, int n){
        var chars = string.toCharArray();
        var encodedChars = new char[chars.length];
        for (int i = 0; i < chars.length; i++) {
            encodedChars[i] = encodeChar(chars[i],e,n);
        }
        return String.copyValueOf(encodedChars);
    }

    private char encodeChar(char ch,int e, int n) {
        var bigIntCh = BigInteger.valueOf(ch);
        var bigChPowE = bigIntCh.pow(e);
        var bigN = BigInteger.valueOf(n);
        var result = bigChPowE.mod(bigN);
        if (ch=='а'||ch=='у') System.out.println(result.toString());
        return (char) result.intValue();
    }
}