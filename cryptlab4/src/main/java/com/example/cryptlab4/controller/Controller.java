package com.example.cryptlab4.controller;

import com.example.cryptlab4.encoder.Decoder;
import com.example.cryptlab4.encoder.Encoder;
import com.example.cryptlab4.encoder.KeyGenerator;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@AllArgsConstructor
public class Controller {

    private final KeyGenerator keyGenerator;
    private final Encoder encoder;
    private final Decoder decoder;

    @GetMapping("generateKeys")
    public Map<String, Map<String, Integer>> Key(
            @RequestParam int p,
            @RequestParam int q,
            @RequestParam int e,
            @RequestParam int d) {
        return keyGenerator.generateKeys(p, q, e, d);
    }

    @GetMapping("encode")
    public String encode(
            @RequestParam String string,
            @RequestParam int e,
            @RequestParam int n) {
        return encoder.encodeString(string, e, n);
    }

    @GetMapping("decode")
    public String decode(
            @RequestParam String encodedString,
            @RequestParam int d,
            @RequestParam int p,
            @RequestParam int q) {
        return decoder.decodeString(encodedString, d, p, q);
    }
}
